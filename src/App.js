import React, {useEffect, useState} from 'react';
import './App.css';
import Recipe from './Recipes'; 

const App = () => {
  // put your own APP_ID and APP_KEY!
  // create an account in edamam to access there food api and keys here:https://edamam.com 
  const APP_ID = ''; 
  const APP_KEY = '';

  // UserState
  const [recipes, setRecipes] = useState([]);
  const [search, setSearch]= useState('');
  const[query, setQuery]= useState('chicken');

  useEffect(() => {
    getRecipes();
  }, [query]);

// getRecipies component 
  const getRecipes = async () => {
    const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`)
    const data = await response.json();
    setRecipes(data.hits);
    console.log(data.hits);
  };

  // updateSearch component 
  const updateSearch = e => {
    setSearch(e.target.value);
    // console.log(search);
  };

  // getSearch component 
  const getSearch = e => {
    e.preventDefault();
    setQuery(search);
    setSearch('');

  }

  return(
    <div className='App'> 
      <form onSubmit={getSearch} className='search-form'>
        <input className= 'search-bar' type='text' value={search} onChange={updateSearch}/>
        <button className= 'search-button' type='submit'>Search</button>
        </form>
        <h1 className='heading'>WELCOME TO K-EATS, WHERE YOUR FAVOURITE RECIPES ARE FOUND!</h1>

        <div className='recipes'>
        {recipes.map(recipe =>(
          <Recipe 
          key={recipe.recipe.label}
          title={recipe.recipe.label}
          calories={recipe.recipe.calories}
          image={recipe.recipe.image}
          ingredients={recipe.recipe.ingredients}
        />
        ))};
        </div>

        
    </div>

  );


};

export default App;
